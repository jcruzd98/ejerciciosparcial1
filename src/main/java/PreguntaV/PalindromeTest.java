/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package PreguntaV;

import java.util.Scanner;

/**
 *
 * @author Josué
 */
public class PalindromeTest {

    public static void main(String args[]){
     
        Scanner scanner = new Scanner(System.in);      
        //int number = scanner.nextInt();
        int[] numbers = {1, 20, 22, 102, 101, 1221, 13321, 13331, 0, 11};
 
        for(int number: numbers){
            System.out.println("Does number : "
               + number +" is a palindrome? " + isPalindrome(number));
        }            
    }

    private static boolean isPalindrome(int number) {
        if(number == reverse(number)){
            return true;
        }
        return false;
    }
 
     
    private static int reverse(int number){
        int reverse = 0;
                                            //voltear el elemento 
        while(number != 0){                      //hacemos operaciones sucesivas para sacar el residuo
          reverse = reverse*10 + number%10;      //de esta manera podemos ir invirtiendo el número que está ingresando
          number = number/10;
        }
             
        return reverse;
    }
    
}//Fin Clase
