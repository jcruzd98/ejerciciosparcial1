package PreguntaIV;

/**
 *
 * @author Josué
 */
public class LinkedListTest {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {

        //creando LinkedList con 5 elementos incluyendo la cabeza
        LinkedList linkedList = new LinkedList();
        LinkedList.Node head = linkedList.head();
        linkedList.add(new LinkedList.Node("1")); //usamos el metodo add para agreagar elementos
        linkedList.add(new LinkedList.Node("2"));
        linkedList.add(new LinkedList.Node("8"));
        linkedList.add(new LinkedList.Node("4"));
        linkedList.add(new LinkedList.Node("5"));

        

        //encontrar el elemento medio de LinkedList en un solo paso
        LinkedList.Node current = head;
        int length = 0;  //inicializamos longitud en 0
        LinkedList.Node middle = head;

        while (current.next() != null)  //se detendrá hasta que el siguiente nodo sea nulo
        {
            length++;
            if (length % 2 == 0)    
            {                       
                middle = middle.next(); 
            }
            current = current.next();
        }

        if (length % 2 == 1)
        {
            middle = middle.next();
        }

        System.out.println("Tamaño de LinkedList: " + length);
        System.out.println("Elemento medio de LinkedList : " + middle);

    }

}

class LinkedList {

    /*Esto es la funcionalidad de linkendList, donde prácticamente se establece
    una cabeza y una cola que servirán como identificadores de los nodos para
    determinar totales y poder manipular la lista*/
    
    private Node head;
    private Node tail;

    public LinkedList() {
        this.head = new Node("cabeza");
        tail = head;
    }

    public Node head() {
        return head;
    }

    public void add(Node node) {
        tail.next = node;
        tail = node;
    }
    
    public static class Node {

        private Node next; // nuestro puntero
        private String data; //nuestro contenedor de datos

        public Node(String data) {
            this.data = data;       //nuestro constructor añade una parametro que almacenará los valores que se agregan a los nodos.
        }

        public String data() {
            return data;    //para que nos devuelva el contenido de data según el nodo que se solicite
        }

        public void setData(String data) {
            this.data = data;   //para poder enviar datos 
        }

        public Node next() {
            return next;        //regresará el enlace al siguiente nodo 
        }

        public void setNext(Node next) {
            this.next = next;   //para poder enviar datos 
        }

        public String toString() {
            return this.data;
        }
    }

}//FIN CLASE
