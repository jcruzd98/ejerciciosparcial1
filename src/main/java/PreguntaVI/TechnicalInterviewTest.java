package PreguntaVI;

import java.util.Arrays;

/**
 * Java program to remove duplicates from this array. You don't need to
 * physically delete duplicate elements, replacing with null, or empty or
 * default value is ok.
 *
 * @author http://javarevisited.blogspot.com
 */
public class TechnicalInterviewTest {

    public static void main(String args[]) {

        int[][] test = new int[][]
        {
            {
                1, 1, 2, 2, 3, 4, 5
            },
            {
                1, 1, 1, 1, 1, 1, 1
            },
            {
                1, 2, 3, 4, 5, 6, 7
            },
            {
                1, 2, 1, 1, 1, 1, 1
            },
        };

        System.out.println("*************** ESPACIOS CON 0 SON INDICES ELIMINADOS ****************");

        for (int[] input : test)
        {
            System.out.println("Matriz con duplicados            : " + Arrays.toString(input));
            System.out.println("Matriz con duplicados borrados   : " + Arrays.toString(removeDuplicates(input)));
        }
    }

    /*   
     Método para eliminar duplicados de la matriz en Java, sin usar
     Clases de colección, clases e.g. Set o ArrayList. Algoritmo para esto
     El método es simple, primero ordena la matriz y luego compara las adyacentes.
     objetos, omitiendo los duplicados, que ya está en el resultado.
     */
    public static int[] removeDuplicates(int[] numbersWithDuplicates) {

        // Clasificación de matriz para reunir duplicados      
        Arrays.sort(numbersWithDuplicates);

        int[] result = new int[numbersWithDuplicates.length];
        int previous = numbersWithDuplicates[0];
        result[0] = previous;

        for (int i = 1; i < numbersWithDuplicates.length; i++)
        {
            int ch = numbersWithDuplicates[i];

            if (previous != ch)
            {
                result[i] = ch;
            }
            previous = ch;
        }
        return result;

    }
}//Fin Clase
