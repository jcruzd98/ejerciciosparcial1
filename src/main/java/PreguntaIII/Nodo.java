/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PreguntaIII;

/**
 *
 * @author Josué
 */
public class Nodo {
    //Referenciamos con punteros al nodo siguiente y a diferencia de las listas enlazadas simples 
    //aquí podemos volver al nodo anterior.
    Object dato;
    Nodo siguiente;
    Nodo anterior;
    
}//Fin Clase
