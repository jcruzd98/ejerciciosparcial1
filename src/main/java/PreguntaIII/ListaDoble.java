/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PreguntaIII;

/**
 *
 * @author Josué
 */
public class ListaDoble {

    Nodo primero;
    Nodo ultimo;

    public ListaDoble() {

        //esto para decir que inicialmente no tendremos nodos.
        primero = null;
        ultimo = null;

    }

    public void ingresarNodo(Object valor) { //Como argumento recibimos los datos para cada nodo usando la variable valor.

        Nodo nuevo = new Nodo();
        nuevo.dato = valor; //vinculamos los datos de ingreso a nuestro nodo.

        if (primero == null)
        {
            //Aquí decimos que si no hay nodos creados, crearemos uno nuevo.
            //Al agregar el primer Nodo tendremos null <- nodo -> null, esto es lo que establecemos con nuestra condición.
            primero = nuevo;
            primero.siguiente = null;
            primero.anterior = null;
            ultimo = primero;

        } else
        {

            //aquí agregamos nuevos nodos y le especificamos que antes de el tendrá ya un nodo pero después de el será nulo.
            //null <- nodo1 -> nodo2 <- null
            ultimo.siguiente = nuevo;
            nuevo.anterior = ultimo;
            nuevo.siguiente = null;
            ultimo = nuevo;

        }

    }

    public void muestraLista() {

        Nodo actual = new Nodo();
        actual = primero;  //nos ubicamos entre los nodos iniciando desde el primer nodo agregado
        
        while(actual != null){ //mientras actual sea distinto a null podremos recorrer nuestra lista
        
            System.out.println(" " + actual.dato.toString()); //imprimimos datos
            actual = actual.siguiente; //recorremos entre espacios en la memoria dinámica donde se almacena cada nodo.
            
        }
        
    }

}//Fin Clase
