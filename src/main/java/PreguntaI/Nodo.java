/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PreguntaI;

/**
 *
 * @author Josué
 */
public class Nodo {
    
    Object valor;
    Nodo siguiente;
    
    public Nodo(Object valor){
     
        //con este constructor asignamos a nuestro puntero un valor
        //prácticamente le decimos que el valor que alojaremos será
        //lo que almacenemos en la varible tipo Object llamada valor.
        
        this.valor = valor;
        this.siguiente = null; //Esto es para que no sea necesario que inicie con un valor en si.
    
    }
    
    public Object obtenerValor(){
    
        //Este método nos devolverá el valor que hemos asignado a la
        //variable que hemos declarado tipo Object
        
        return valor;
    
    }
    
    public void enlazarSiguiente(Nodo n){
    
        //Este método lo que nos permite es poder apuntar a otro nodo
        //que nosotros creamos desde afuera, es decir creará otro espacio
        //en memoria que nos dirigirá al próximo dato almacenado.
        
        siguiente = n;
        
    }
    
    public Nodo obtenerSiguiente(){ 
    
        //Este método lo que hará es devolvernos el enlace
        //al siguiente nodo que hayamos solicitado.
        
        return siguiente;
    
    }
            
}//Fin Clase
