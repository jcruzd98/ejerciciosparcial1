/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package com.joe.primerparcial;

import PreguntaI.Nodo;
import PreguntaII.ListaCircular;
import PreguntaIII.ListaDoble;

/**
 *
 * @author Josué
 */
public class home {

    /**
     * @param args the command line arguments
     */
    public static void primeraPregunta() {

        Nodo primero = new Nodo("Enero");
        Nodo segundo = new Nodo(105);
        Nodo tercero = new Nodo("Febrero");

        primero.enlazarSiguiente(segundo);
        primero.obtenerSiguiente().enlazarSiguiente(tercero);

        System.out.println(primero.obtenerSiguiente().obtenerSiguiente().obtenerValor().toString());
        //System.out.println(segundo.obtenerSiguiente().obtenerValor().toString());
        //System.out.println(primero); //de esta manera podemos ver el espacio en memoria donde se está alojando el tercer nodo.

    }

    public static void segundaPregunta() {

        ListaCircular ingresa = new ListaCircular();

        ingresa.ingresarNodo("Hola");
        ingresa.ingresarNodo(155);
        ingresa.ingresarNodo("Este es el Nodo 3");
        ingresa.ingresarNodo("Buen día");
        ingresa.ingresarNodo(2022);

        ingresa.muestraLista(); //llamamos al método que despliega nuestra lista circular.

    }

    public static void terceraPregunta() {

        ListaDoble control = new ListaDoble();

        // null <-
        control.ingresarNodo("Soy el primer nodo");
        control.ingresarNodo(2022);
        control.ingresarNodo(348.7);
        control.ingresarNodo("Soy el cuarto nodo");
        control.ingresarNodo("Hola Mundo");
        control.ingresarNodo(24);
        // ->null

        control.muestraLista();

    }

    public static void main(String[] args) {
        /* --- Este proyecto contendrá todo lo solicitado en el primer parcial
        del curso de Programación III --- */

        //primeraPregunta();
        //segundaPregunta();
        //terceraPregunta();

    }

}//Fin Clase Home
