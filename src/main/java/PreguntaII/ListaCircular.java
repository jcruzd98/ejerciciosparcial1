/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PreguntaII;

/**
 *
 * @author Josué
 */
public class ListaCircular {
    
    Nodo primero;
    Nodo ultimo;
    
    public ListaCircular(){
        //incializamos nuestros punteros en el constructor diciendoles que tendrán
        //un valor null para iniciar.
        
        primero = null;
        ultimo = null;
        
    }
    
    public void ingresarNodo(Object valor){
    
        Nodo nuevo = new Nodo();
        nuevo.dato = valor;
        
        if(primero == null){
        
            primero = nuevo;
            ultimo = primero;
            primero.siguiente = ultimo; //avanzamos entre nodos pero ubicandolo en memoria, usando el puntero siguiente
            
        }else{
        
            //Con esto decimos que si ya hay nodos que avance en memoria creando uno nuevo
            //e indicandole que siempre erá detrás del primero y así el último será el dato
            //que acabamos de ingresar en el nuevo nodo.
            
            ultimo.siguiente = nuevo;
            nuevo.siguiente = primero;
            ultimo = nuevo;
            
        }
        
    }
    
    public void muestraLista(){
    
        //creamos una variable tipo Nodo llamada actual para poder recorrer la lista circular.
        Nodo actual = new Nodo();
        actual = primero; //indicamos que actual será igual al primero para poder iniciar el recorrido desde el inicio.
        
        do{
        
            System.out.println(" " + actual.dato.toString());
            actual = actual.siguiente;
            
        }while (actual != primero); //iremos avanzando y mostrando hasta que actual vuelva a ser igual a primero.
        
    }
    
    
}//Fin Clase
